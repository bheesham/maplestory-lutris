Tried to get MapleStory (MapleSaga+MapleLegends) working on Lutris: failed
miserably. Here's how far I got.

You'll need to find and download:

* `ws2help.dll` [scan results](https://www.virustotal.com/gui/file/85ea67e265dd678ce5fe245860df8fb12e8a36b485451dc7393d4babdf52f640)
* `ws2_32.dll`: [scan results](https://www.virustotal.com/gui/file/e33cc4bc94094588394f8f29b772c0ac2c919f48b2939b1d140a2d759b708038)
* `ijl15.dll`: [scan results](https://www.virustotal.com/gui/file/334aa12f7dee453d1c6cb1b661a3bb3494d3e4cc9c2ff3f9002064c78404e43a)
* the MapleSaga/MapleLegends installer

DLLS can be downloaded [here](https://gofile.io/d/9bWBQT), if it's still alive.

Verify you got the right DLLs by running:

```
$ sha256sum dlls-orig/ws2help.dll dlls-orig/ws2_32.dll dlls-orig/ijl15.dll
85ea67e265dd678ce5fe245860df8fb12e8a36b485451dc7393d4babdf52f640  dlls-orig/ws2help.dll
e33cc4bc94094588394f8f29b772c0ac2c919f48b2939b1d140a2d759b708038  dlls-orig/ws2_32.dll
334aa12f7dee453d1c6cb1b661a3bb3494d3e4cc9c2ff3f9002064c78404e43a  dlls-orig/ijl15.dll
```

Note that whatever DLL you download you should probably run through a virus
scanner anyways, just to be super safe.
